<html>
    <head>
        <title>Brevet Controle Times</title>
    </head>

    <body>
        <h1>Brevet Controle "/listAll" Times</h1>
        <h2>Open Time:</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
	          $open = $obj->open_times;
            foreach ($open as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h2>Close Times:</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
	          $close = $obj->close_times;
            foreach ($close as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

        <h1>Brevet Controle "/listOpenOnly/csv" Times</h1>
        <h2>Open Time:</h2>
        <ul>
            <?php
            $csv_string = file_get_contents('http://laptop-service/listOpenOnly/csv');
            $arr = explode("\n", $csv_string);
            foreach ($arr as $l) {
                if ($l != NULL)
                    echo "<li>$l</li>";
            }
            ?>
        </ul>

        <h1>Brevet Controle "/listCloseOnly/json?top=3" Times</h1>
        <h2>Top 3 Close Time:</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service//listCloseOnly/json?top=3');
            $obj = json_decode($json);
	          $close = $obj->close_times;
            foreach ($close as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

    </body>
</html>
